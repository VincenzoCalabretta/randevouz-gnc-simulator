# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/home/v/projDinamica/src/_deps/matplotlib-src"
  "/home/v/projDinamica/src/_deps/matplotlib-build"
  "/home/v/projDinamica/src/_deps/matplotlib-subbuild/matplotlib-populate-prefix"
  "/home/v/projDinamica/src/_deps/matplotlib-subbuild/matplotlib-populate-prefix/tmp"
  "/home/v/projDinamica/src/_deps/matplotlib-subbuild/matplotlib-populate-prefix/src/matplotlib-populate-stamp"
  "/home/v/projDinamica/src/_deps/matplotlib-subbuild/matplotlib-populate-prefix/src"
  "/home/v/projDinamica/src/_deps/matplotlib-subbuild/matplotlib-populate-prefix/src/matplotlib-populate-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/home/v/projDinamica/src/_deps/matplotlib-subbuild/matplotlib-populate-prefix/src/matplotlib-populate-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "/home/v/projDinamica/src/_deps/matplotlib-subbuild/matplotlib-populate-prefix/src/matplotlib-populate-stamp${cfgdir}") # cfgdir has leading slash
endif()
