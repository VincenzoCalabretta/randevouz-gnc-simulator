project(simulator)
cmake_minimum_required(VERSION 3.25)

#to generate configuration json for clangd, execute cmake as follows:
#cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1
set(CMAKE_BUILD_TYPE Debug)

# set build directory
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../bin)
message(STATUS "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")
# guard against in-source builds
#if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
#    message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there. You may need to remove CMakeCache.txt. ")
#endif()

# ---- Eigen -------------------------------------------------------------------
find_package (Eigen3 3.3 REQUIRED NO_MODULE)
# ---- gnuplot-iostream --------------------------------------------------------
include_directories ("./gnuplot-iostream/")
# ---- Add and link to target --------------------------------------------------
add_executable(simulator simulator.cpp)
# link python and numpy
target_link_libraries(simulator
    PRIVATE
    Eigen3::Eigen
)
