#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <boost/numeric/odeint.hpp>
#include "eigen/Eigen/Core"
#include "eigen/Eigen/Geometry"
#include "eigen/Eigen/src/Core/Matrix.h"
#include "eigen/Eigen/src/Geometry/Quaternion.h"
#include "gnuplot-iostream/gnuplot-iostream.h"
// -----------------------------------------------------------------------------
//              %%% CONFIGURATION %%%
// -----------------------------------------------------------------------------

//typedef std::vector< double > state_type;
typedef Eigen::Matrix<double, 3, 1> state_vect;
typedef Eigen::Matrix<double, 4, 1> state_quat;
typedef Eigen::Matrix<double, 6, 1> state_esa;

#define COUNT_GUIDANCE 5 // control guidance and control execution frequency
#define COUNT_CONTROL 2
#define COUNT_PLANT 1
// -----------------------------------------------------------------------------
//              %%% CLASSES %%%
// -----------------------------------------------------------------------------
//[ logger and plotter
class sys_logger {
public:
    #define PUSH_SAVE(name,time) push_save(#name, (name),(time))
    // save double
    void push_save(std::string name,double vin,double time){
    std::string log_pth = "../logs/"+name;
    std::ofstream log_str;
    log_str.open (log_pth,std::ios_base::app);
    log_str << time << " " << vin << "\n";
    log_str.close();
    }
    // save Eigen::Vector3d
    void push_save(std::string name,Eigen::Vector3d vin, double time){
    std::string log_pth = "../logs/"+name;
    std::ofstream log_str;
    log_str.open (log_pth,std::ios_base::app);
    log_str << time
        << " " << vin(0)
        << " " << vin(1)
        << " " << vin(2)
        << "\n";
    log_str.close();
    }
    // save Eigen::Vector4d
    void push_save(std::string name,Eigen::Vector4d vin, double time){
    std::string log_pth = "../logs/"+name;
    std::ofstream log_str;
    log_str.open (log_pth,std::ios_base::app);
    log_str << time
        << " " << vin(0)
        << " " << vin(1)
        << " " << vin(2)
        << " " << vin(3)
        << "\n";
    log_str.close();
    }
    #define GNUPLOT_OPTS "with lines "

    #define PRINTER(name) printer(#name)
    void printer(std::string name){
        Gnuplot gp;
        gp << "set term png \n";
        gp << "set output '../plots/" << name<< ".png' "<< "\n" ;
        std::string log_pth = "../logs/"+name;
        gp << "plot for [col=2:5] '" << log_pth << "' using 1:col " << GNUPLOT_OPTS << "\n";
    }
    private:
};
//]
//[ euler_dynamic_ode
class euler_dyn {
public:
    Eigen::Vector3d Mb;
    euler_dyn( Eigen::Matrix3d i_I) : I(i_I), inv_I(i_I.inverse()) {
    }
    void operator() (const state_vect &x , state_vect &dxdt , const double /* t */ )
    {
        dxdt = inv_I*(Mb-x.cross(I*x));
    }
private:
    Eigen::Matrix3d I;
    Eigen::Matrix3d inv_I;
};
//]
//[ euler_dynamic_ode_rw
class euler_dyn_rw {
public:
    Eigen::Vector3d Mb_ext = {0,0,0};
    Eigen::Vector3d Mb_rw;
    Eigen::Vector3d h_rw;
    euler_dyn_rw( Eigen::Matrix3d i_I) : I(i_I), inv_I(i_I.inverse()) {
    }
    void operator() (const state_vect &x , state_vect &dxdt , const double /* t */ )
    {
        dxdt = inv_I*(Mb_ext +Mb_rw -x.cross(I*x+h_rw));
    }
private:
    Eigen::Matrix3d I;
    Eigen::Matrix3d inv_I;
};
//]


//[ quaternions kinematics ode
class quaternions_kin {
public:
    state_vect omega_b;
    Eigen::Matrix4d A;
    quaternions_kin(){}
    void operator() (const state_quat &q , state_quat &dqdt , const double /* t */ )
    {
        A <<             0,  -omega_b(0),    -omega_b(1),    -omega_b(2),
                omega_b(0),            0,     omega_b(2),    -omega_b(1),
                omega_b(1),  -omega_b(2),              0,     omega_b(0),
                omega_b(2),   omega_b(1),    -omega_b(0),              0;

        dqdt = 0.5 * A*q;
    }
    state_quat dqdt(state_quat q){
        //in some way odeint clears both A and the other variables, exept the one
        //set by outside (omega_b)
        A <<             0,  -omega_b(0),    -omega_b(1),    -omega_b(2),
                omega_b(0),            0,     omega_b(2),    -omega_b(1),
                omega_b(1),  -omega_b(2),              0,     omega_b(0),
                omega_b(2),   omega_b(1),    -omega_b(0),              0;
        return 0.5 * A*q;
    }
private:
};
//]

//[ euler angles kinematics ode
// non e' necessario risolvere ode kinematica con angoli di eulero, in quanto
// omega e' input e angoli di eulero sono ottenuti dai quaternioni risultanti
// dalla soluzione della kinematica con quaternioni.
// Quindi si sfrutta la soluzione ottenuta con la soluzione della kin in quaternioni
// per calcolare la variazione degli angoli di eulero.
// angoli di eulero+ variazione angoli di eulero permettono il calcolo della
// legge di controllo PD in angoli di eulero
Eigen::Vector3d euler_angles_kin(Eigen::Vector3d omega_b, Eigen::Vector3d ea){
    double c1 = cos(ea(0)/2);
    double c2 = cos(ea(1)/2);
    double c3 = cos(ea(2)/2);
    double s1 = sin(ea(0)/2);
    double s2 = sin(ea(1)/2);
    double s3 = sin(ea(2)/2);
    Eigen::Matrix3d A;
        A << c2,     s1*s2,      c1*s2,
              0,     c1*c2,     -s1*c2,
              0,        s1,         c1;

        return (A*omega_b)/c2;
};
//]

//[ hill_ode
class hill_dyn {
public:
    double mc; //mass of the chaser;
    double wot; //orbital omega target;
    Eigen::Vector3d F; //Linear thrust applied;
    hill_dyn() {
    }
    void operator() (const state_esa &x , state_esa &dxdt , const double /* t */ )
    {
        //bisognerebbe vedere se cambiando ordine funziona
        /*
        dxdt(0) = F(0)/mc + 2*wot*x(2);
        dxdt(1) = F(1)/mc - pow(wot,2)*x(4);
        dxdt(2) = F(2)/mc - 2*wot*x(0) + 3*pow(wot,2)*x(5);
        dxdt(3) = x(0);
        dxdt(4) = x(1);
        dxdt(5) = x(2);

        dxdt(0) = F(0)/mc;
        dxdt(1) = F(1)/mc;
        dxdt(2) = F(2)/mc;
        dxdt(3) = x(0);
        dxdt(4) = x(1);
        dxdt(5) = x(2);
        */
        // state vector contains first velocities and last position
        dxdt(0) = F(0)/mc + 2*wot*x(2);
        dxdt(1) = F(1)/mc - pow(wot,2)*x(4);
        dxdt(2) = F(2)/mc - 2*wot*x(0) + 3*pow(wot,2)*x(5);
        dxdt(3) = x(0);
        dxdt(4) = x(1);
        dxdt(5) = x(2);
        }
    private:
    };
//]

//[ quaternion PRINTER
#define Q_PRINT(name) q_print(#name, (name))
void q_print(std::string name, state_quat q){
    std::cout << "Printing: " << name << " :" <<
        q(0) << " " <<
        q(1) << " " <<
        q(2) << " " <<
        q(3) << " " << std::endl;
}
//]


//[ quaternion conjugate
state_quat q_conjugate(state_quat q_in){
    state_quat q_out = {q_in(0),-q_in(1),-q_in(2),-q_in(3)};
    return q_out;
}
//]

//[ quaternion inverse
state_quat q_inverse(state_quat q_in){
    state_quat q_out;
    q_out = q_conjugate(q_in)/q_in.squaredNorm();
    return q_out;
}
//]

//[ quaternion attitude error (as rotation)
state_quat q_error(state_quat q_des, state_quat q_true){
    //calculate the inverse of q_des
    state_quat inv_q_des = q_inverse(q_des);
    //quaternion moltiplication
    state_quat q = inv_q_des;
    Eigen::Matrix<double, 4,4> A;
    A <<    q(0),-q(1),-q(2),-q(3),
            q(1), q(0),-q(3), q(2),
            q(2), q(3), q(0),-q(1),
            q(3),-q(2), q(1), q(0);
    //return quaternion error
    return A*q_true;
}
//]
//[ quaternion diff component wise (do not use)
state_quat q_diff(state_quat q_des, state_quat q_true){
    state_quat q;
    q << q_des - q_true;
    //return quaternion diff component wise
    return q;
}
//]




//[ quaternion numerical error q_error
double comp_quaternion_error(state_quat quaternion){
    double quaternion_error = quaternion.squaredNorm()-1;
    return quaternion_error;
};
//]

//[ from quaternions to Euler angles
Eigen::Vector3d q2eul(Eigen::Vector4d q){
    Eigen::Vector3d eul_ang;
    eul_ang(0) = atan2( 2*(q(0)*q(1) + q(2)*q(3)), 1 - 2*(pow(q(1),2) + pow(q(2),2)));
    eul_ang(1) = -M_PI/2 + 2*atan2(
                                    sqrt(1 + 2*(q(0)*q(2) - q(1)*q(3))),
                                    sqrt(1 - 2*(q(0)*q(2) - q(1)*q(3)))
                                   );
    eul_ang(2) = atan2( 2*(q(0)*q(3) + q(1)*q(2)), 1 - 2*(pow(q(2),2) + pow(q(3),2)));
    return eul_ang;
};
//]

//[ from Euler Angles to quaternions
Eigen::Vector4d eul2q(Eigen::Vector3d ea){
    Eigen::Vector4d q;
    double c1 = cos(ea(0)/2);
    double c2 = cos(ea(1)/2);
    double c3 = cos(ea(2)/2);
    double s1 = sin(ea(0)/2);
    double s2 = sin(ea(1)/2);
    double s3 = sin(ea(2)/2);

    q(0) =c3*c2*c1 + s3*s2*s1;
    q(1) =c3*c2*s1 - s3*s2*c1;
    q(2) =c3*s2*c1 + s3*c2*s1;
    q(3) =s3*c2*c1 - c3*s2*s1;

    return q;
};
//]

//[ transformation matrix from I to B
int comp_L_BI(Eigen::Matrix3d& L_BI,state_quat qc){
    Eigen::Vector3d q;
    double q0;
    Eigen::Matrix3d Q_til;

    q << qc[1],qc[2],qc[3];
    q0 = qc[0];
    Q_til << 0, -qc[3], qc[2],
            qc[3], 0, -qc[1],
            -qc[2], qc[1], 0;

    Eigen::Matrix3d Id_3; // Eventually remove in rework
    Id_3 = Eigen::Matrix3d::Identity();
    L_BI = (pow(q0,2) - q.dot(q))*Id_3 + 2*q*q.transpose() - 2 * q0 * Q_til;
    return 1;
}
//]

//[ angular momentum in B axis
Eigen::Vector3d comp_angular_momentum_B(state_vect omega_b, Eigen::Matrix3d I){
    return I*omega_b;
};
//]
//[ angular momentum in B axis
double comp_kinetic_energy_B(state_vect omega_b, Eigen::Vector3d h_b){
    return 0.5*omega_b.dot(h_b);
};
//]

//[ proportional derivative control law
class control_law_pd{
    public:
        state_vect omega_b;
        state_quat q_error;
        state_quat dqdt;
        state_vect ea_error;
        state_vect deadt;
        control_law_pd(Eigen::Matrix<double,3,3> I_i,double zeta_i,double omega_nat_des_i)
        :I(I_i),zeta(zeta_i),omega_nat_des(omega_nat_des_i){

        };
        int control_law_quat(Eigen::Vector3d& u){
            Eigen::Vector3d I_vect = {I(0,0),I(1,1),I(2,2)};
            Eigen::Vector3d k_id = 2 * zeta * omega_nat_des * I_vect;
            Eigen::Vector3d k_ip = pow(omega_nat_des,2) * I_vect;
            Eigen::Vector3d q_ie = {q_error(1),q_error(2),q_error(3),};
            Eigen::Vector3d dqdt_ie = {dqdt(1),dqdt(2),dqdt(3),};
            double q_0 = q_error(0);
            for (int i=0; i<3 ;i++){
                u(i) = - ( k_id(i)*omega_b(i) + k_ip(i)*q_ie(i));
            }
            return 1;
        }
        int control_law_ea(Eigen::Vector3d& u){
            Eigen::Vector3d I_vect = {I(0,0),I(1,1),I(2,2)};
            Eigen::Vector3d k_d = 2 * zeta * omega_nat_des * I_vect;
            Eigen::Vector3d k_p = pow(omega_nat_des,2) * I_vect;
            for (int i=0; i<3 ;i++){
                u(i) = (- k_d(i)*deadt(i) + k_p(i)*ea_error(i));
            }
            return 1;
        }
        Eigen::Matrix<double,3,3> I;
        double zeta;
        double omega_nat_des;
    private:
};
//]

//[ actuator control modulator
// in a sound software ar5chitecture shold include from derivation/dependancy injection
class actuator_modulation{
    public:
        actuator_modulation(double momentum_cap_i, double deadzone_i,double time_step_i)
            : momentum_cap(momentum_cap_i), deadzone(deadzone_i), time_step(time_step_i) {
            //determine filter factor from time_step
            //initilization
            double tau=1;
            double Kf=0.05;
            factor = exp(-time_step/tau);
            std::cout << "factor :"<< factor << std::endl;
            am = {0,0,0};
            la_am = am;
            am_rw = {0,0,0,0};
            la_am_rw = am_rw;
            //Shmidt characteristics
            double delta =0.2;
            u_on = delta*Kf;
            u_off = momentum_cap*Kf*(exp(-0.02/tau)-1)+u_on;
            //
            double delta_t_on = - tau *log(1- (u_on-u_off)/(momentum_cap*Kf));
            std::cout << "u_off :"<< u_off << std::endl;
            std::cout << "delta_t_on :"<< delta_t_on << std::endl;
            }
        int saturated(std::vector<Eigen::Vector3d> &Mb,Eigen::Vector3d control_momentum,int sim_it){
            Eigen::Vector3d actuator_momentum;
            // check input control momentum component wise
            for(int i=0; i<3 ; i++){
               if(control_momentum(i) > deadzone/2){
                    actuator_momentum(i) = momentum_cap;
               }else if (control_momentum(i) < -deadzone/2){
                    actuator_momentum(i) = -momentum_cap;
                }
            }
            // set torque for the current control time frame
            for(int i=0; i<=COUNT_CONTROL; i++){
                Mb[sim_it+i] = actuator_momentum;
            }
            return 1;
        }
        int pwpf(std::vector<Eigen::Vector3d> &Mb,Eigen::Vector3d control_momentum,int sim_it){
            Eigen::Vector3d actuator_momentum;
            // am == actuator momentum error (control input - effective momentum)
            am = control_momentum - Mb[sim_it]; // feedback subtraction of the previous effective momentum
            // low pass filter (leaky integrator)
            am = lowPassExponential(am,la_am); //actuator momentum

            //iterates Shmidt trigger for every component
            for (int i=0; i<3; i++){
                actuator_momentum(i) =  0;
                if ( am(i) > la_am(i)){ // Rightwise
                    if (am(i) < -u_off)  {
                       actuator_momentum(i) =  -momentum_cap;
                    }
                    if (am(i) > u_on){
                       actuator_momentum(i) =  momentum_cap;
                    }
                }else if(am(i) < la_am(i)){ // Leftwise
                    if (am(i) < -u_on)  {
                       actuator_momentum(i) =  -momentum_cap;
                    }
                    if (am(i) > u_off){
                       actuator_momentum(i) =  momentum_cap;
                    }
                }
            }
            // set torque for the current control time frame
            for(int i=0; i<=COUNT_CONTROL; i++){
                Mb[sim_it+i] = actuator_momentum;
            }
            //set last actuator momentum error
            la_am = am ;
            return 1;
        }
        int reaction_wheel(std::vector<Eigen::Vector3d> &Mb,Eigen::Vector3d& h_3rw,
                Eigen::Vector3d control_momentum,int sim_it){
        Eigen::Vector3d actuator_momentum;
        Eigen::Vector4d actuator_momentum_4rw;
        Eigen::Vector4d delta_h_4rw;

        Eigen::Matrix<double,3,4> Z;
        Eigen::Matrix<double,4,3> Z_pinv;
        const double alpha =0;
        const double beta  =0.523599;
        double ca = cos(alpha);
        double cb = cos(beta);
        double sa = sin(alpha);
        double sb = sin(beta);
        Z << cb*ca,     -cb*sa,     -cb*ca,      cb*sa,
             cb*sa,      cb*ca,     -cb*sa,     -cb*ca,
             sb,         sb,         sb,         sb;
        Z_pinv = Z.completeOrthogonalDecomposition().pseudoInverse();
        am_rw= Z_pinv * control_momentum;
        am_rw = lowPassExponential(am_rw,la_am_rw); //actuator momentum 4RW
        //saturation
        for(int i=0; i<4 ; i++){
           if(am_rw(i) > momentum_cap){
                actuator_momentum_4rw(i) = momentum_cap;
           }else if (am_rw(i) < -momentum_cap){
                actuator_momentum_4rw(i) = -momentum_cap;
            }else{
                actuator_momentum_4rw(i) = am_rw(i);
            }
        }
        //deadzone
        for(int i=0; i<4 ; i++){
           if(am_rw(i) > -deadzone/2 && am_rw(i) < deadzone/2){
                actuator_momentum_4rw(i) = 0;
            }
        }
        logger.PUSH_SAVE(actuator_momentum_4rw,sim_it*time_step); //assuming initial time is zero
        delta_h_4rw = actuator_momentum_4rw*time_step;
        actuator_momentum = Z*actuator_momentum_4rw;
        h_3rw += Z*delta_h_4rw;

        // set torque for the current control time frame
        for(int i=0; i<=COUNT_CONTROL; i++){
            Mb[sim_it+i] = actuator_momentum;
        }
        //set last actuator momentum error
        la_am_rw = am_rw ;
        return 1;
        }
        int passthrough(std::vector<Eigen::Vector3d> &Mb,Eigen::Vector3d control_momentum,int sim_it){
            // set torque for the current control time frame
            for(int i=0; i<=COUNT_CONTROL; i++){
                Mb[sim_it+i] = control_momentum;
            }
            return 1;
        }

        template<typename eigen_v>
        eigen_v lowPassExponential(eigen_v input, eigen_v prev_out){
            return prev_out*factor + (1-factor)*input;  // ensure factor belongs to  [0,1] 
        }
        Eigen::Vector3d am;
        Eigen::Vector3d la_am;
        Eigen::Vector4d am_rw;
        Eigen::Vector4d la_am_rw;
        double momentum_cap;
        double deadzone;
        double time_step;
        double factor;
        double u_on;
        double u_off;
        sys_logger logger;
    private:
};
//]


//[ manouver effectuation domain structure
struct eff_domain{
    state_quat q;
    int if_q[4];

    state_vect s;
    int if_s[3];

    state_vect v;
    int if_v[3];

    double t;
    int if_t;
};

//[ manouver effectuation domain structure
struct schedule{
    double man_start_time;
    double man_duration; //control is retained during the duration of the manouver
};
//]
//[ manouver structure
struct manouver_data{
    int id;
    state_quat attitude;
    state_vect velocity;
    double duration;
    eff_domain manouver_req;
};
//]


//[ calculate hohmann manouver
// control timer is set by a manouver and decreases every time the control step
// is executed. If it is negative the guidance funciton is executed again

class manouver{
public:
    double wot;
    double max_thrust;
    double mc;
    int executed;

    state_vect s;
    state_vect v;

    double control_retain_factor = 1.5;
    int m_step; //controls execution flow of the manouver
    double control_timer;

    Eigen::Vector3d delta_V;
    Eigen::Vector3d ph_attitude;

    manouver(){
           m_step = 0;
    }

    int change_attitude(state_quat& q_des, state_vect expected_ea_attitude){
        // set desired attitude change
        q_des = eul2q(expected_ea_attitude);
        // set control timer
        control_timer = 60; //s , arbitrary at the moment
        return 1;
    }
    int change_velocity(state_vect& v_des, state_vect delta_V){
        //module which schedules a sm to change velocity
        //satellite pointing is expected
        //sets desidered velocity of the control algorithm
        v_des = v + delta_V;
        //sets control timer
        control_timer =mc*delta_V.norm()/max_thrust;
        return 1;
    }

    int plan_hohmann(state_quat& q_des, state_vect& v_des, state_esa trasl_state){
        //copy traslational dynamics state into specific vectors
        for (int i=0; i<3; i++) {
            v(i) = trasl_state(i);
            s(i) = trasl_state(i+3);
        }
        double delta_x = abs(s(0));
        double delta_y = abs(s(1));
        double delta_z = abs(s(2));
        double tolerance = 100; //meters
        double condition;

        if (control_timer < 0.0){
            // the first time guidance notices control has no more control,
            // resets contol timer until a new operation is set and
            // sets the switch to the thereafter procedure
            control_timer = 0.0;
            m_step++;
        }
            //switch is executed only if guidance has control
            switch (m_step) {
                case 0://thrust calculation
                    //calculate delta_V
                    delta_V(0) = wot*delta_z/4;
                    delta_V(1) = 0;
                    delta_V(2) = 0;
                    //calculate required attitude
                    ph_attitude = {0,0,0}; //placeholder attitude vector
                    //directly execute next step
                    control_timer = -1;
                break;

                case 1:// fist attitude change
                    if (!(control_timer > 0))
                    change_attitude(q_des,ph_attitude);
                break;

                case 2:// fist velocity change
                    if (!(control_timer > 0))
                    change_velocity(v_des, delta_V);
                break;

                case 3: //wait for the spacecraft to reach desidered position
                        //there shall be a flag to select/lock control
                    condition = -abs(0 - s(2)) + tolerance;
                    if (condition > 0 ){
                        control_timer=-1;
                    } else {
                        control_timer=9;
                    }
                break;

                case 4:// second attitude change
                    if (!(control_timer > 0))
                    change_attitude(q_des,ph_attitude);
                break;

                case 5:// second attitude change
                    if (!(control_timer > 0))
                    change_velocity(v_des, delta_V);
                break;
            }
        //velocity is always considered in the LVLH reference fram
        //determine attitude necessary to perform burn by pointing


        //we should divide the hohmann transfer in two manouver
        //since each manouver requires firstly a change in attitude and than a change
        //in velocity, we should divide each of the previous manouvers into two separate
        //manouvers
        // hohmann --- first burn attitude change
        //          |- first burn velocity change
        //          |- second butn attitude change
        //          |- second burn velocity change
        //we can thus identify two separate modules: attitude change and velocity change.
        //each execution shall be characterized by initial time and duration
        //if some kind of logic is needed, another module is used to change the schedule to NOW.

        return 1;
    }
private:
};
//]


// -----------------------------------------------------------------------------
//              %%% MAIN %%%
// -----------------------------------------------------------------------------

int main(void){
    using namespace boost::numeric::odeint;
    //calculate the inverse of q_des
    using namespace Eigen;

    //[remove previous logfiles
    system("rm ../logs/*");
    system("rm ../plots/*.png");
    //

    //[simulation parameters
    // ONLY INITIAL CONDITIONS MUST BE INITIALIZED, others DECLARATION ONLY
    //simulation parameters evaluated at compile time;
    constexpr double time_init = 0.0; //s
    constexpr double time_fin = 10.0; //s
    constexpr double time_step = 0.01; //s, relative to 100Hz plant evaluation
    double time = time_init; //s, execution current step time
    constexpr int  num_steps= (time_fin-time_init)/time_step;

    //initial angular velocity
    state_vect omega_b; omega_b << 0.0, 0.0, 0.0; // rad/s
    //initial attitude
    state_quat q; q << 0.5, 0.5, 0.5, 0.5;
    //Tensor of inertia
    Matrix3d I; I <<    15  , 0     , 0 , // kg/m^2
                        0   , 19    , 0 ,
                        0   , 0     ,25 ;
    //orbital omega target;
    double wot = 0.5; //rad /s 0.5
    //initial mass of the chaser
    double mc = 10; // Kg

    // velocity and position vectors derived from translational dynamics state vector
    state_vect s; s<< -10000.0 ,0.0 ,3000.0 ;
    state_vect v; v<< 2250.0, 0.0 ,0.0;
    // transltional dynamics state vector
    state_esa trasl_state; trasl_state << v(0),v(1),v(2),s(0),s(1),s(2);

    // controller configuration
    double zeta = 1.5; //1.5
    double omega_nat_des = 0.2; //0.2

    // targets, initially set as initial conditions;
    Eigen::Vector3d ea_des;
    state_quat q_des; q_des = q;
    state_vect v_des; v_des = v;

    //thruster and actuator confuguration
    double momentum_cap = 0.01; //Nm //0.2
    double thrust_cap = 1; //N
    Vector3d h_rw; h_rw << 0,0,0; //reaction wheels initial state
    //double deadzone = 0.25/momentum_cap; //Nm //0.25* (180.0/M_PI)
    double deadzone = 0.001; //Nm //0.25



    //[ Objects initialization
    //Applied momentum and force (initialized for every time frame);
    std::vector<Vector3d> Mb(num_steps,Vector3d::Zero());
    std::vector<Vector3d> F(num_steps,Vector3d::Zero());
    //attitude variation
    state_quat dqdt;
    state_quat q_err;
    //attitude in Euler angles
    state_vect ea;
    state_vect ea_degrees;
    state_vect deadt;
    state_vect deadt_degrees;
    state_vect ea_err;
    state_vect ea_err_degrees;
    // traslational dynamics error
    state_vect v_err;


    //
    Eigen::Vector3d Mb_contr;
    //quaternion numeric error
    double q_num_error;
    //Moment of inertia;
    Vector3d h_b;
    double h_b_modulus;
    Vector3d h_i;
    double h_i_modulus;
    //kinetic enery
    double tau;
    //Transformation matrixes
    Eigen::Matrix3d L_BI;
    Eigen::Matrix3d L_IB;
    //initialization of systems for ODE solvers.
    euler_dyn eu(I);
    euler_dyn_rw eu_rw(I);
    quaternions_kin qt;
    hill_dyn  he;
    //initialization of steppers for ODE solvers.
    runge_kutta4< state_vect > rk4_vect;
    runge_kutta4< state_quat > rk4_quat;
    runge_kutta4< state_esa > rk4_esa;
    //initialization of control laws
    control_law_pd cl(I,zeta,omega_nat_des);
    //initialization of actuation modulators
    actuator_modulation am(momentum_cap,deadzone,time_step);
    //initialization of loggers
    sys_logger logger;

    //initialization of manouvers
    double control_timer = 0;// general control timer for all manouvers
    manouver ho_1;
    ho_1.wot =wot;
    ho_1.max_thrust = thrust_cap;
    ho_1.mc = mc;
    ho_1.executed = 0; //manouver has been executed flag
    //]

    for (int sim_it = 0; sim_it<num_steps; sim_it++){
        //execute guidance algorithms
        if(sim_it%COUNT_GUIDANCE ==0 && sim_it != 0){
            if (ho_1.executed == 0){
                ho_1.control_timer = control_timer;
                ho_1.plan_hohmann(q_des, v_des, trasl_state); //consider passing
                                                              //control timer as par.
                control_timer = ho_1.control_timer;
                logger.PUSH_SAVE(q_des,time);
                logger.PUSH_SAVE(v_des,time);
                logger.PUSH_SAVE(control_timer,time);
            }
            //other manouvers go here
        }

        //execute control algorithms
        if(sim_it%COUNT_CONTROL ==0 && sim_it != 0){
            // deternine attitude error and control law momentum
            q_err = q_error(q_des, q);
            ea_err = ea_des - ea;

            cl.omega_b = omega_b;
            cl.q_error = q_err;
            cl.dqdt = dqdt;
            //cl.ea_error = ea_err;
            //cl.deadt = deadt;
            cl.ea_error = ea_err_degrees;
            cl.deadt = deadt_degrees;
            cl.control_law_quat(Mb_contr);
            //cl.control_law_ea(Mb_contr);
            //am.saturated(Mb, Mb_contr,sim_it);
            //am.pwpf(Mb, Mb_contr,sim_it);
            am.reaction_wheel(Mb, h_rw, Mb_contr, sim_it);
            //am.passthrough(Mb, Mb_contr,sim_it);

            //decrease control_timers of manouvers
            control_timer =- COUNT_CONTROL*time_step;
        }
        //execute plant evaluation
        if(sim_it%COUNT_PLANT ==0){

            //assign forces and momentum for the current time frame, update
            //paraneters like inertia tensor if necessary

            //F[sim_it](0) = 10;
            // (1) attitude dynamics
            eu.Mb = Mb[sim_it];
            eu_rw.Mb_rw =Mb[sim_it];
            eu_rw.h_rw =h_rw;
            he.F = F[sim_it];
            he.wot = wot;
            he.mc = mc;
            //solve euler equation
            rk4_vect.do_step(eu_rw, omega_b, time, time_step);
            //solve quaternion kinematics ode
            qt.omega_b=omega_b;
            rk4_quat.do_step(qt, q, time, time_step);
            dqdt = qt.dqdt(q);  //dqdt not necessary
            //obtain kinematics description in ea
            ea = q2eul(q);
            deadt =euler_angles_kin(omega_b,ea);

            //(2) translational dynamics
            rk4_esa.do_step(he,trasl_state,time,time_step);
            //copy traslational dynamics state into specific vectors
            for (int i=0; i<3; i++) {
                v(i) = trasl_state(i);
                s(i) = trasl_state(i+3);
            }
            // ---- visualization telemetries -----
            // euler angles in degrees
            ea_degrees = ea * (180.0/M_PI);
            deadt_degrees = deadt * (180.0/M_PI);
            ea_err_degrees = ea_err * (180.0/M_PI);

            //calculate quaternion numerical error
            q_num_error = comp_quaternion_error(q);
            //calculate transformation matrixes between reference frames
            comp_L_BI(L_BI,q);
            L_IB = L_BI.transpose();
            //calculate moment of inertial
            h_b=comp_angular_momentum_B(omega_b,I);
            h_i=L_IB*h_b;
            h_b_modulus = h_b.norm();
            h_i_modulus = h_i.norm();
            //calculate kinetic energy
            tau = comp_kinetic_energy_B(omega_b,h_b);
        }
    //update time
    time += time_step;
    //execute observer logging (for every timeframe)
    //time is updated before logging, becouse parameters are calculated for the
    //end of the timeframe
    logger.PUSH_SAVE(h_b,time);
    logger.PUSH_SAVE(h_b_modulus,time);
    logger.PUSH_SAVE(h_i,time);
    logger.PUSH_SAVE(h_i_modulus,time);
    logger.PUSH_SAVE(omega_b,time);
    logger.PUSH_SAVE(q,time);
    logger.PUSH_SAVE(dqdt,time);
    logger.PUSH_SAVE(q_num_error,time);
    logger.PUSH_SAVE(tau,time);
    logger.PUSH_SAVE(ea,time);
    logger.PUSH_SAVE(deadt,time);
    logger.PUSH_SAVE(ea_degrees,time);
    logger.PUSH_SAVE(deadt_degrees,time);
    logger.PUSH_SAVE(ea_err,time);
    logger.PUSH_SAVE(ea_err_degrees,time);
    logger.PUSH_SAVE(ea_degrees(0),time);
    logger.PUSH_SAVE(ea_degrees(1),time);
    logger.PUSH_SAVE(ea_degrees(2),time);
    logger.PUSH_SAVE(Mb[sim_it],time);
    logger.PUSH_SAVE(Mb[sim_it](0),time);
    logger.PUSH_SAVE(Mb[sim_it](1),time);
    logger.PUSH_SAVE(Mb[sim_it](2),time);
    logger.PUSH_SAVE(F[sim_it],time);
    logger.PUSH_SAVE(q_err,time);
    logger.PUSH_SAVE(Mb_contr,time);
    logger.PUSH_SAVE(trasl_state(0),time);
    logger.PUSH_SAVE(trasl_state(1),time);
    logger.PUSH_SAVE(trasl_state(2),time);
    logger.PUSH_SAVE(trasl_state(3),time);
    logger.PUSH_SAVE(trasl_state(4),time);
    logger.PUSH_SAVE(trasl_state(5),time);
    // traslational trajectory visualization
    double LVLH_S = s(2);
    logger.PUSH_SAVE(LVLH_S,s(0));
    }
    //execute visualization
    logger.PRINTER(h_b);
    logger.PRINTER(h_b_modulus);
    logger.PRINTER(h_i);
    logger.PRINTER(h_i_modulus);
    logger.PRINTER(omega_b);
    logger.PRINTER(q);
    logger.PRINTER(dqdt);
    logger.PRINTER(q_num_error);
    logger.PRINTER(tau);
    logger.PRINTER(ea);
    logger.PRINTER(deadt);
    logger.PRINTER(ea_degrees);
    logger.PRINTER(deadt_degrees);
    logger.PRINTER(ea_err);
    logger.PRINTER(ea_err_degrees);
    logger.PRINTER(ea_degrees(0));
    logger.PRINTER(ea_degrees(1));
    logger.PRINTER(ea_degrees(2));
    logger.PRINTER(Mb[sim_it]);
    logger.PRINTER(Mb[sim_it](0));
    logger.PRINTER(Mb[sim_it](1));
    logger.PRINTER(Mb[sim_it](2));
    logger.PRINTER(F[sim_it]);
    logger.PRINTER(q_err);
    logger.PRINTER(Mb_contr);
    logger.PRINTER(actuator_momentum_4rw);
    logger.PRINTER(trasl_state(0));
    logger.PRINTER(trasl_state(1));
    logger.PRINTER(trasl_state(2));
    logger.PRINTER(trasl_state(3));
    logger.PRINTER(trasl_state(4));
    logger.PRINTER(trasl_state(5));
    logger.PRINTER(LVLH_S);
    logger.PRINTER(q_des);
    logger.PRINTER(v_des);
    logger.PRINTER(control_timer);
}
