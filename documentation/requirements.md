# List of top level requirements for the project


### Functional requirements
#### Exercise 0:
    0.1. Obtain graph of attitude quaternions
    0.2 Obatin graph of angular velocities
    0.3 Obtain graph of the quaternion error
    0.4 Obtain graph of magnitude of angular momentum
    0.5 Obrain graph of angular momentum components
    0.6 Obtain graph of kinetic energu
    0.7_add Obtain graph of x and DxDt relative to free drift
#### Exercise 1:
    1.1 Obtain graph of Torque components in body frame
    1.2 Obtain graph of Euler angles
    1.3 Obtain graph of Angular momentum comoponents in inertial frame
    1.4 Obtain graph of angular momentum compoents in body frame
    1.5 Calculate a posterirori fuel consumption and manouver time
#### Exercise 2:
    2.1 Obrain graph of controller torque
    2.1 Obrain graph of effective torque
#### Exercise 3:
    3.1 Obtain graph of  torque components

### Implementation requirements
#### Exercise 0:
#### Exercise 1:
#### Exercise 2:
    2.1 Implement a PD control class
    2.1 Implement a modular PWPF modulator that uses thruster confguration as input
#### Exercise 3:
    3.1 Implement a QFC control class
    3.2 Implement a reaction wheel actuator model
